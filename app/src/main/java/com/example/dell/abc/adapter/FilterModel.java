package com.example.dell.abc.adapter;

/**
 * Created by DELL on 7/20/2017.
 */

public class FilterModel {

    private String tag;
    private boolean flag;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}
