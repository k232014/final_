package com.example.dell.abc.adapter;

import android.content.ClipData;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dell.abc.MainActivity;
import com.example.dell.abc.R;
import com.example.dell.abc.fragment.Tags;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.R.attr.country;
import static android.R.attr.tag;
import static android.R.id.message;
import static com.example.dell.abc.MainActivity.database;
import static com.example.dell.abc.R.id.snap;

/**
 * Created by DELL on 7/20/2017.
 */

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.ViewHolder>  {



    private Context context;
    private java.util.List<FilterModel> list = new ArrayList<>();


    public FilterAdapter(Context context, java.util.List<FilterModel> list)
    {
        this.context=context;
        this.list=list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.tags_row,parent,false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mFiltername.setText(list.get(position).getTag());

       if(list.get(position).isFlag())
           holder.isFav.setChecked(true);
        else
           holder.isFav.setChecked(false);

        holder.isFav.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean b) {
                if(buttonView.isChecked())
                {
                    list.get(position).setFlag(true);
                }
                else {
                    list.get(position).setFlag(false);
                }
            }
        });


    }
    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView mFiltername;
        CheckBox isFav;


        public ViewHolder(View itemView) {
            super(itemView);

            mFiltername=(TextView) itemView.findViewById(R.id.txt_name);
            isFav=(CheckBox) itemView.findViewById(R.id.checkBox);

        }
    }
}



