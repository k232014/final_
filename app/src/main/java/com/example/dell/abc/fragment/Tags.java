package com.example.dell.abc.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dell.abc.R;
import com.example.dell.abc.adapter.FilterAdapter;
import com.example.dell.abc.adapter.FilterModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.L;
import static com.example.dell.abc.MainActivity.database;

/**
 * A simple {@link Fragment} subclass.
 */
public class Tags extends Fragment {
    RecyclerView recyclerView;
    java.util.List<FilterModel> l1 = new ArrayList<>();
    public static Map<String,Boolean> userdata= new HashMap<>();
    Button btnupdate;
    DatabaseReference myRef;
    public Tags() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view=inflater.inflate(R.layout.fragment_tags, container, false);

        btnupdate=(Button)view.findViewById(R.id.btncount);
        recyclerView=(RecyclerView)view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        final FilterAdapter filterAdapter=new FilterAdapter(getContext(),l1);
        recyclerView.setAdapter(filterAdapter);

        myRef = database.getReference("user/123/tags");
        myRef.addValueEventListener(new ValueEventListener() {


            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                //database.auth().currentUser.getToken(true);
                l1.clear();
                for (DataSnapshot snapshot:dataSnapshot.getChildren()) {
                    Log.v("SNAPSHOT_VALUE", snapshot.getValue().toString());
                    String key = snapshot.getKey();
                    boolean flag = (boolean) snapshot.getValue();
                    userdata.put(key,flag);
                }
                setTagsDataToAdapter(userdata);
                }

            @Override
            public void onCancelled(DatabaseError error) {

            }
        });
        btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i=0; i<l1.size();i++)
                {
                    Log.v("tags",l1.get(i).getTag());
                    DatabaseReference tagref= myRef.child(l1.get(i).getTag());
                    tagref.setValue(l1.get(i).isFlag());
                }
            }
        });
        return view;
    }

    private void setTagsDataToAdapter(final Map<String, Boolean> userdata) {

        for(String key: userdata.keySet()){
            FilterModel filterModel=new FilterModel();
            filterModel.setTag(key);


            if(!userdata.get(key))
                filterModel.setFlag(false);

            else
                filterModel.setFlag(true);
                l1.add(filterModel);


            recyclerView.getAdapter().notifyDataSetChanged();
        }

    }
}
